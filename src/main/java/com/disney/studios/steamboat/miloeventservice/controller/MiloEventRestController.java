package com.disney.studios.steamboat.miloeventservice.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api
@RestController
@RequestMapping(value="/milo")
public class MiloEventRestController {

    @ApiOperation(value = "Echo request body")
    @ApiResponses({
            @ApiResponse(code = 400, message = "request is bad"),
            @ApiResponse(code = 500, message = "unhandled server exception")
    })
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value="/echo", consumes= MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> echoRequest(@RequestBody String payload) {
        return new ResponseEntity<>(payload, HttpStatus.OK);
    }

    @ApiOperation(value = "Hello world endpoint")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value="/hello", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> helloRequest() {
        return new ResponseEntity<>("Hello world of milo event service :)", HttpStatus.OK);
    }

}


