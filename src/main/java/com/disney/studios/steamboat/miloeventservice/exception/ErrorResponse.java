package com.disney.studios.steamboat.miloeventservice.exception;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.http.HttpStatus;

@Value
@RequiredArgsConstructor
public class ErrorResponse {

    private final HttpStatus httpStatus;
    private final String errorCode;
    private final String description;

    public ErrorResponse(RestApiException e) {
        this.httpStatus = e.getHttpStatus();
        this.errorCode = e.getErrorCode();
        this.description = e.getDescription();
    }
}
