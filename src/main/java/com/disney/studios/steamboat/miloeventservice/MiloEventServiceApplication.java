package com.disney.studios.steamboat.miloeventservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiloEventServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiloEventServiceApplication.class, args);
	}
}
